# Battleship

This is simple implementation of Battleship game https://en.wikipedia.org/wiki/Battleship_(game).

Assumption:
+ Board 10x10
+ Human against computer
+ Computer anchors ships
+ Player shots
+ Ships:
  + 1 x Battleship (5 squares)
  + 2 x Destroyers (4 squares)
+ Ships can touch but not overlap
+ Player input in format ColRow Col(A-J), Row(1-10) ex. A5
+ Board
  + "." - no-shot
  + "-" - miss
  + "X" - hit

## Installation

    $ fetch repo
    $ ./bin/battleship
