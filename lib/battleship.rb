require "battleship/version"

require "battleship/board"
require "battleship/shipyard"
require "battleship/pirate"

module Battleship
  class Game

    WELCOME_MSG = 'Welcome in BattleshipGame.'

    def initialize
      @board = Board.new
      blackbeard = Pirate.new(@board)

      blackbeard.hide_ship(Shipyard.create(:battleship))
      blackbeard.hide_ship(Shipyard.create(:destroyer))
      blackbeard.hide_ship(Shipyard.create(:destroyer))

      @mesg = WELCOME_MSG + help
    end

    def start
      loop do
        show_you_won if @board.all_ships_cnt == @board.sank_ships_cnt
        rewind_screen
        show_stats
        show_board
        show_mesg
        cmd = user_prompt
        @mesg = case cmd
        when /^[A-Ja-j](10|[1-9])$/
          x = Board::COLUMN_NAMES.index(cmd[0].upcase)
          y = cmd[1..-1].to_i - 1
          @board.hit(x, y)
        when 'show'
          @board.show_ships
          'ARRRGHHHH. You cheater! ;)'
        else
          "Unknown command #{cmd}." + help
        end
      end
    rescue Interrupt
      rewind_screen
      puts "\nIf you have to leave i will show you my board.\n"
      @board.show_ships
      show_board
      show_stats
      exit
    end

    private

      def finished?
        false
      end

      def help
        'Move format [A-J][1-10] ex. A1, J10'
      end

      def show_stats
        puts '---------------------------------------------'
        puts "Shots: #{@board.shots}, Destroyed ships: #{@board.sank_ships_cnt}, Left: #{@board.floating_ships_cnt}"
        puts '---------------------------------------------'
      end

      def show_board
        @board.display
      end

      def user_prompt
        print "Your move > "
        gets.chomp
      end

      def show_mesg
        puts @mesg
        @mesg = ''
      end

      def rewind_screen
        100.times { puts "\n" } # stupid rewinding of screen to simulate change
      end

      def show_you_won
        puts "YEAH ! YOU SANK ALL VESSELS, YOU WON!"
        show_stats
        exit
      end
  end
end
