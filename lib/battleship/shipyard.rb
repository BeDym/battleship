require "battleship/ship"
require "battleship/warship"
require "battleship/destroyer"

module Battleship
  class Shipyard

    def self.create(ship_type)
      case ship_type
      when :battleship
        return Warship.new
      when :destroyer
        return Destroyer.new
      else
        raise "Unknown ship type"
      end
    end
  end
end
