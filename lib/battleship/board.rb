require "battleship/cell"
require "battleship/impact"

module Battleship
  class Board
    attr_accessor :rows, :cols, :shots

    ROWS = 10
    COLS = 10
    COLUMN_NAMES = %w(A B C D E F G H I J)

    def initialize
      @grid = Array.new(ROWS) { Array.new(COLS) { Cell.new } }
      @rows = ROWS
      @cols = COLS
      @shots = 0
      @ships = []
    end

    def hit(x, y)
      @shots += 1
      cell = cell(x, y)
      cell.hit
      if cell.state == Battleship::Cell::TAKEN
        if cell.ship.floating?
          return Impact::StillFloating.new
        else
          return Impact::HitAndSank.new
        end
      end
      Impact::Missed.new
    end


    def ship_fits_horizontal?(x, y, ship)
      (x..(x+(ship.size-1))).each do |new_x|
        return false if cell(new_x, y).taken?
      end
      true
    end

    def ship_fits_vertical?(x, y, ship)
      (y..(y+(ship.size-1))).each do |new_y|
        return false if cell(x, new_y).taken?
      end
      true
    end

    def anchor_ship_horizontal(x, y, ship)
      (x..(x+(ship.size-1))).each do |new_x|
        cell(new_x, y).ship = ship
      end
      @ships << ship
    end

    def anchor_ship_vertical(x, y, ship)
      (y..(y+(ship.size-1))).each do |new_y|
        cell(x, new_y).ship = ship
      end
      @ships << ship
    end

    def show_ships
      (0..ROWS-1).each do |y|
        (0..COLS-1).each do |x|
          cell(x, y).discover
        end
      end
    end

    def all_ships_cnt
      @ships.size
    end

    def floating_ships_cnt
      cnt = 0
      @ships.each{|ship| cnt += 1 if ship.floating? }
      cnt
    end

    def sank_ships_cnt
      cnt = 0
      @ships.each{|ship| cnt += 1 unless ship.floating? }
      cnt
    end

    private

      def cell(x, y)
        @grid[y][x]
      end

      def to_s
        ret = '  ' + columns_str + "\n"
        @grid.each_with_index do |row, index|
          ret << "#{index+1} "
          row.each do |cell|
            ret << " #{cell} "
          end
          ret << "\n"
        end
        ret
      end

      def columns_str
        ret = ''
        COLUMN_NAMES.each{|c| ret << " #{c} "}
        ret
      end
  end
end
