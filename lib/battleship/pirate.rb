module Battleship
  class Pirate

    def initialize(sea)
      @sea = sea
    end

    def hide_ship(ship)
      x_min = 0
      x_max = @sea.cols - 1
      y_min = 0
      y_max = @sea.rows - 1
      ship_orientation = [:vertical, :horizontal][rand(0..1)]

      # TODO: make better checking of available space, prevent from looping
      if ship_orientation == :horizontal
        x_max = x_max - ship.size
        find_and_anchor_horizontal((x_min..x_max), (y_min..y_max), ship)
      else
        y_max = y_max - ship.size
        find_and_anchor_vertical((x_min..x_max), (y_min..y_max), ship)
      end
    end

    def random_coordiante(x_range, y_range)
      x = rand(x_range.min..x_range.max)
      y = rand(y_range.min..y_range.max)
      [x, y]
    end

    private

      def find_and_anchor_horizontal(range_x, range_y, ship)
        x, y = random_coordiante(range_x, range_y)
        while !@sea.ship_fits_horizontal?(x, y, ship)
          x, y = random_coordiante(range_x, range_y)
        end
        @sea.anchor_ship_horizontal(x, y, ship)
      end

      def find_and_anchor_vertical(range_x, range_y, ship)
        x, y = random_coordiante(range_x, range_y)
        while !@sea.ship_fits_vertical?(x, y, ship)
          x, y = random_coordiante(range_x, range_y)
        end
        @sea.anchor_ship_vertical(x, y, ship)
      end
  end
end
