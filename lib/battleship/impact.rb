module Battleship
  class Impact
    def to_s
      self.class::MESSAGE
    end

    class Missed < Impact
      MESSAGE = '--- OH no we missed :('
    end

    class StillFloating < Impact
      MESSAGE = '--- ARGGGH Captain!. We HIT but vessel is still floating.'
    end

    class HitAndSank < Impact
      MESSAGE = '--- ARGGGH Captain!. We SANK THEM!'
    end
  end
end
