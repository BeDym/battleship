module Battleship
  class Cell
    attr_accessor :state, :ship

    def initialize
      @state = Cell::HIDDEN
      @ship = nil
      @cell_hit = false
    end

    def discover
      @state = @ship.nil? ? Cell::EMPTY : Cell::TAKEN
    end

    def hit
      if !@ship.nil? && @cell_hit == false
        @ship.hit
        @cell_hit = true
      end
      discover
    end

    def taken?
      !@ship.nil?
    end

    private

      def to_s
        @state::MARKER
      end
  end

  class Cell::HIDDEN;
    MARKER = '.'
  end

  class Cell::EMPTY
    MARKER = '-'
  end

  class Cell::TAKEN
    MARKER = 'X'
  end
end
