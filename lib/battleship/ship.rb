module Battleship
  class Ship
    attr_accessor :hits

    def initialize
      @hits = 0
    end

    def floating?
      @hits < self.class::SIZE
    end

    def hit
      @hits += 1
    end

    def size
      self.class::SIZE
    end
  end
end
